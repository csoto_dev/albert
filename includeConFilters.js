'use strict';
//desde el front haz el get a esta url
//http://localhost:3000/api/experiences?filter={"include": [{"relation":"expStatus", "scope":{"where": {"idExpStatus": "1"}}}]}

module.exports = function(Experience) {
    ModelName.afterRemote('find', cleaningResponse);
    
    asyn function cleaningResponse(ctx){
        try{
            //aqui asigno lo que me trajo de la DB.
            let dirtyResponse = ctx.result;
            //aqui asigno lo que envie en el include el array que ves alli arriba 
            let include = JSON.parse(ctx.req.query.filter).include;
            //aqui por cada relacion que envie en el array filtro dirtyResponse 
            //y lo asigno a si mismo para reemplazar lo ya filtrado
            for(let relation of include){
                dirtyResponse = dirtyResponse.filter(item => item.__data[relation.relation] != undefined);
            }

            // asigno el dirtyResponse ya limpio con los filtrados al result para que lo devuelva
            ctx.result = dirtyResponse;
                        
        }catch (err){
            throw err;
        }
    }
};