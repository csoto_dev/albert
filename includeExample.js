'use strict';

module.export = function(ExperienciasModel) {
    
    //si el modelo es llamado desde otro modelo, o anidado usa observe
    ModelName.observe('before save', funcionALlamar);
    
    async function funcionALlamar(ctx){
        try{
            //code to do

            let result = await ExperienciasModel.findOne({ 
                include: [
                    {
                      relation: 'galleries'
                    },
                    {
                      relation: 'userContactData'
                    },
                    {
                      relation: 'threatTypes',
                      scope: {
                        include: ''
                      }
                    },
                    {
                      relation: 'guilds'
                    },
                    {
                      relation: 'adaptLevels',
                      scope: {
                        include: ''
                      }
                    },
                    {
                      relation: 'locationDepartments',
                      scope: {
                        include: ''
                      }
                    },
                    {
                      relation: 'request'
                    },
                    {
                      relation: 'measureTypes'
                    },
                    {
                      relation: 'project'
                    },
                    {
                      relation: 'entity'
                    },
                    {
                      relation: 'expLongName'
                    },
                    {
                      relation: 'indicator'
                    },
                    {
                      relation: 'privacy'
                    },
                    {
                      relation: 'expStatus'
                    }
                  ]
            });

            
            return departamentoData;
            
        }catch (err){
            throw err;
        }
    }

}