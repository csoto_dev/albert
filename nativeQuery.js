'use strict';
//este archivo debe ir en una carpeta llamada services en common quedando el path "common/services" 

const app = require('./../../server/server');

module.exports = {

    execute: async (query) => {  
        const dataSource = app.datasources.mainDB;
                
        return new Promise(function(resolve, reject) {
            dataSource.connector.execute(query, [], function(err, units) {
                if (err) {
                    return reject(err);
                }
                return resolve(units);
            });
        });    
    }
    
};