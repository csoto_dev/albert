'use strict';

const nativeQuery = require('./../services/nativeQuery');

module.exports = function(ModelName) {
    //si el modelo es llamado desde otro modelo, o anidado usa observe
    ModelName.observe('before save', funcionALlamar2);
    
    asyn function funcionALlamar2(ctx){
        try{
            //code to do
            let query = 'SELECT * FROM ModelName';
            
            const result = await nativeQuery.execute(query);
            
            return result;
            
        }catch (err){
            throw err;
        }
    }
};
